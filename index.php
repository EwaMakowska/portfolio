<!DOCTYPE html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Moje portfolio</title>
<meta content="Ewa, Makowska, strony www, grafika komputerowa" name="keywords">

<link rel="Stylesheet" type="text/css" href="css/reset.css" />
<link rel="Stylesheet" type="text/css" href="css/style.css" />
<link rel="Stylesheet" type="text/css" href="css/rwd.css" />
<link rel="Stylesheet" type="text/css" href="css/responsiveslides.css" />
<link rel="Stylesheet" type="text/css" href="subpages/about/css/about.css" />
<link rel="Stylesheet" type="text/css" href="subpages/gallery/css/gallery.css" />
<link rel="Stylesheet" type="text/css" href="subpages/gallery/css/rwd.css" />
<link rel="Stylesheet" type="text/css" href="subpages/webs/css/webs.css" />
<link rel="Stylesheet" type="text/css" href="subpages/webs/css/rwd.css" />
<link rel="Stylesheet" type="text/css" href="subpages/apps/css/apps.css" />
<link rel="Stylesheet" type="text/css" href="subpages/apps/css/rwd.css" />

<link href="https://fonts.googleapis.com/css?family=Coiny|Supermercado+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/responsiveslides.min.js"></script>

<script>
  $(function() {
    $(".rslides").responsiveSlides();
  });
</script>

<script>
  $(document).ready(function() {
    $("#text").load("subpages/about/about.php");
  })
</script>

<script type="text/javascript" src="translate/longTextTranslate.js"></script>
<script type="text/javascript" src="translate/translate.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script>
  $(document).ready(function(){
      Translator().setLang();
  })
</script>
</head>

<body>

<div id="container">

	<div id="baner">
    <ul class="rslides">
      <li>
        <img src="img/slideshow/gallery.png">
        <div>
          <p class="translate">galeria</p>
        </div>
      </li>
      <li>
          <img src="img/slideshow/website.png">
          <div>
            <p class="translate">strony www</p>
          </div>
      </li>
      <li>
          <img src="img/slideshow/applications.png">
          <div>
            <p class="translate">apki</p>
          </div>
      </li>
    </ul>
    <div id="teleadr">
      <div>
  			<p>e-mail:<a href="mailto:makowskaewa@o2.pl"> makowskaewa&#64;o2.pl</a>&nbsp; &#124; &nbsp; tel: 607079560</p>
      </div>
      <div>
        <span class="lang active" data-lang="pl">
          <img src="icons/pllang.png">
        </span>
  			<span class="lang" data-lang="eng">
          <img src="icons/anglang.png">
        </span>
      </div>
		</div>
	</div>
  <!-- header end -->

	<div id="content" class="clearfix">
      <div id="infbox">
        <div>
          <span id="menu" class='link' data-subpage="about">menu</span>
        </div>
        <div>
          <ul>
            <li>
              <div id="about" class="bigger link" data-subpage='about'>
                <p class='translate'>o mnie</p>
              </div>
            </li>
            <li>
              <div class="bigger link" data-subpage='gallery'>
                <p class="translate">galeria</p>
              </div>
              <ul>
                <li>photoshop</li>
                <li>gimp</li>
                <li>inscape</li>
                <li>blender - grafika 3D</li>
              </ul>
            </li>
            <li>
              <div class="bigger link" data-subpage='webs'>
                <p class="translate">strony www</p>
              </div>
              <ul>
                <li>html5</li>
                <li>css3</li>
                <li>javaScript</li>
                <li>jquery</li>
                <li>wordPress</li>
              </ul>
            </li>
            <li>
              <div class="bigger link" data-subpage='apps'>
                <p class="translate">apki</p>
              </div>
              <ul>
                <li>php</li>
                <li>mySQL</li>
                <li>javaScript</li>
                <li>nodeJS</li>
                <li>reactJS</li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <div id="text" class="small"></div>
  </div>
  <!-- body end -->
  <footer>
    <p>Makowska Ewa, &nbsp;<a   href="mailto:makowskaewa@o2.pl"> makowskaewa&#64;o2.pl</a>&nbsp; &#124; &nbsp;2012-2017</p>
  </footer>
  <!-- footer end -->
</div>
</body>
</html>
