
  const Presenter = function(){
    var presented, show, fullDesc, cont;

    presented = [
      {
      url: 'demo/colorsGame/',
      name: 'Graj w kolorki!',
      desc: 'Graj w kolorki! Wybierz taki sam kolor, w jakim napisana jest nazwa wylosowanego koloru. Spiesz się, czas ucieka coraz szybciej i szybciej. Uważaj, bo mózg może cię oszukać i uznać za ważniejsze to, co jest napisane, a nie to co widzisz. Zobacz ile punktów jesteś w stanie zdobyć zanim popełnisz trzy błędy. Ćwicz swoją koncentrację.'
    },
    {
      url: 'demo/sampleUserProfile/',
      name: 'Sample User Profil',
      desc: 'Mała próbka możliwości reactJS. Wkonany z użyciem biblioteki reactJS, menadzera pakietów webpack oraz na środowisku nodeJS przykładowy profil użytkownika. Like-uj i obserwuj do woli, a jeśli chcesz, wypowiedz się pod profilem.'
    },
    /*{
      url: 'demos/presenceList/',
      name: 'Lista obecności',
      desc: 'Prosta lista obecności umożliwiająca dodanie obecności zalogowanej osoby, oraz podejrzenie historii. Oparta o angularJS. Backend zrobiony w PHP. SQL-owa baza danych.'
    }*/
  ];

    show = function(url, desc, name) {
      fullDesc =
        "<a href ='" + url + "'>" +
          "<h1 class='translate'>" + name + "</h1>" +
        "</a>" +
        "<div>" +
          "<p class='translate'>" + desc + "</p>" +
        "</div>";

      cont =
        "<div id='webmin' class='clearfix'>" +
          "<div>" +
            fullDesc +
          "</div>" +
          "<div>" +
            "<iframe src='" + url + "' scrolling='no'>" +
              "ups, twoja przeglądarka nie obsługuje ramek" +
            "</iframe>" +
          "</div>" +
        "</div>";

      return cont;
    };

    return {
      show: show,
      presented: presented
    }
  };

  display = function(){
    var len, url, name, desc;

    len = Presenter().presented.length;

    for(let i = 0; i <= len; i++){
      url = Presenter().presented[i].url;
      name = Presenter().presented[i].name;
      desc = Presenter().presented[i].desc;

      $('#insidetext.apps').append(Presenter().show(url, desc, name));
    }
  };
  display();
