const Presenter = function(){
  var presented, show, fullDesc, cont;

  presented = [
    {
    url: 'http://www.fototrendy.com.pl',
    desc: 'Strona oparta o CMS wordPress wzbogacona dla dodatkowego efektu o skrypty jQuery.'
  },
    {
    url: 'http://www.jota-rusztowania.pl',
    desc: 'Strona oparta o drupala. Moją zasługą są dodatkowe skrypy js, mające uczynić ją bardziej efektywną oraz przyjazną dla użytkownika.'
    },
    /*{
    url: 'http://www.zsp5.jaworzno.pl/biblioteka',
    desc: "Stronka robiona dla Biblioteki Zespołu Szkół Ogólnokształcących w Jaworznie. Najpierw na joomli, następnie została przepisana na wordPressa. Wykonana w 100% przez mnie."
  },*/
    {
    url: 'http://www.falconwet.jaw.pl',
    desc: "CMS: joomla, wykonanie, grafika: Ewa Makowska"
    },
    {
    url: 'http://megacom.com.pl',
    desc: "Do wykonania tej stronki nie użyłam żadnego CMS-a. Jest ona oparta o HTML i CSS wsparte javaScriptem, jQuery i różnymi interesującymi pluginami."
  }
  ];

  show = function(url, desc) {
    fullDesc =
      "<a href ='" + url + "'>" +
        "<h1>" + url + "</h1>" +
      "</a>" +
      "<div>" +
        "<p class='translate'>" + desc + "</p>" +
      "</div>";

    cont =
      "<div id='webmin' class='clearfix'>" +
        "<div>" +
          fullDesc +
        "</div>" +
        "<div>" +
          "<iframe src='" + url + "' scrolling='no'>" +
            "ups, twoja przeglądarka nie obsługuje ramek" +
          "</iframe>" +
        "</div>"
      "</div>";

    return cont;
  };

  return {
    show: show,
    presented: presented
  }
};

display = function(){
  var length, url, desc;

  len = Presenter().presented.length;

  for(var i = 0; i <= len; i++){
    url = Presenter().presented[i].url;
    desc = Presenter().presented[i].desc;

    $('#insidetext.webs').append(Presenter().show(url, desc));
  }
}();
