$(document).ready(function(){

  var resize = () => {
    var marginRight;

    if($(window).width() >= 800){
      marginRight = '350px'
    } else { marginRight = '200px'}

    return {marginRight: marginRight}
  }

    $('#album').css('background', 'none');
    $('#text p').css({'float':'left', 'margin-right': resize().marginRight});
    $('.album1 , .album2').css({
      'float':'right',
      '-webkit-transform' : 'rotate(0deg)',
          '-moz-transform' : 'rotate(0deg)',
          '-ms-transform' : 'rotate(0deg)',
          'transform' : 'rotate(0deg)'})
    .animate({
          width: "174px",
          height:"220px",
      }, 1500 )

      $('.album2').animate({
        paddingRight: "10px"
      })
})
