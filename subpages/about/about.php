<div id="insidetext" class="clearfix about">
  <h1 class="translate">Po prostu to lubię!</h1>
  <br><br>
  <div id="album">
    <img src="subpages/about/img/snowboard_skok.jpg" class="album1" />
    <img src="subpages/about/img/w_bibliotece.jpg" class="album2" />
  </div>
  <div class="longTextTranslate">
  <p>Tak, po prostu lubię zajmować się grafiką komputerową i tworzeniem stron www. Pierwszy raz z html-em i css-em zetknęłam się podczas studiów na Uniwersytecie Jagiellońskim. O nie, nie były to studia związane z informatyką, co wcale nie okazało się przeszkodą. Zaczęłam się doszkalać i doszkalać, robić grafiki i strony www dla znajomych oraz ogłaszać przez internet. Poświęciłam czas na poznawanie javascript, mysql i php. Zetknęłam się z&nbsp;Joomlom, najbardziej rozbudowanym i dającym największe możliwości cms-em, który przez pewien czas był moim głównym narzędziem pracy.</p>
  <p>Ale moje życie to nie tylko strony www. Uwielbiam również bieganie, snowboarding oraz książki.</p>
  <p>Zapraszam serdecznie do obejrzenia mojego portfolio.</p>
  </div>
</div>
