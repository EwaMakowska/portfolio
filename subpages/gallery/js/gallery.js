$(document).ready(function() {
  const Gallery = function(){
  	var pictureDivWidthResize, bigImgContent, showImg, nextImg, prevImg, closeImg, key, src, id;

    var pictureDivWidthResize = () => {
      var width;
      if($(window).width() >= 800){
        width = '900px'
      } else {width = '100%'}
      return width
    }

  	bigImgContent = function(src, key){
  		var img =
  			"<div id='prevImg' class='arrow prev'>"+
  				"<img src='subpages/gallery/icons/arrowPrev.png'>"+
  			"</div>"+
  			"<img src="+ src +" id='bigimg' data-id="+ key +">"+
  			"<div id='nextImg' class='arrow'>"+
  				"<img src='subpages/gallery/icons/arrowNext.png'>"+
  			"</div>";

  			return img;
  		}

  		showImg = function(thisImg){
  				key = $('.smallimages').index(thisImg);
  				src = $(thisImg).attr('src');

  				$("#picture").html(bigImgContent(src, key));
  				$('#close, #fixed, #picture, #mask').css('display','block');
  				$('#mask').animate({'width':'100%','height':screen.height +100, opacity:'0.5'});
  				$('#picture').animate({width: pictureDivWidthResize()});
  				$('html, body').animate({scrollTop:"350px"}, 1500);
  		};

  		closeImg = function(){
  				$('#close, #fixed, #picture, #mask').css('display','none');
  		};

  		prevImg = function(){
  			id = Number($('#bigimg').attr('data-id'))-1;
  			if(id >= 0){
  				src = $('.smallimages')[id].getAttribute('src');
  				$("#picture").html(bigImgContent(src, id));
  			}

        if(id == 0) {
          $('#prevImg').css({'display': 'none'});
        }
  		};

  		nextImg = function(){
  			id = Number($('#bigimg').attr('data-id'))+1;
  			if(id <= $('.smallimages').length-1){
  				src = $('.smallimages')[id].getAttribute('src');
  				$("#picture").html(bigImgContent(src, id));
  			}

        if(id == $('.smallimages').length-1){
          $('#nextImg').css({'display': 'none'});
        }
  		};

  		return {
  			showImg: showImg,
  			closeImg: closeImg,
  			nextImg: nextImg,
  			prevImg: prevImg
  		}
  };

  $('.smallimages').click(function(){
  	Gallery().showImg(this);
  });

  $('#close, #fixed').on("click", function(){
  	Gallery().closeImg();
  });

  $('#picture').on("click", '#prevImg', function(e){
  	e.stopPropagation();
  	Gallery().prevImg();
  });

  $('#fixed').on("click", '#nextImg', function(e){
  	e.stopPropagation();
  	Gallery().nextImg();
  });
})
