const Translator = () => {

  var setLang = () => {
    $('.lang').click(function(){
      $('.lang').removeClass("active");
      $(this).addClass("active");
      getTranslation($(this).attr('data-lang'))
    })
  };

  var getTranslate = (data, lang) => {
    longTextTranslate().getTranslate(lang);
    var containers = $('.translate');
    for(let i = 0; i <= containers.length-1; i++){
      var content = containers.eq(i).text();
      $.map(data, (engText, plText) => {
        switch (lang) {
          case "eng":
            if(content == plText){
              containers.eq(i).text(engText);
            }
            break;
          case "pl":
            if(content == engText){
              containers.eq(i).text(plText);
            }
            break;
          default:
            if(content == plText){
              containers.eq(i).text(engText);
            }
        }
      });
    }
  };

  var getTranslation = function(lang){
    $.getJSON('translate/translate.json', function(text) {
       getTranslate(text, lang);
    });
  };

  return {setLang: setLang,
          getTranslation: getTranslation}
}
