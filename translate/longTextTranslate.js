const longTextTranslate = () => {
  var mainPl = "<p>Tak, po prostu lubię zajmować się grafiką komputerową i tworzeniem stron www. Pierwszy raz z html-em i css-em zetknęłam się podczas studiów na Uniwersytecie Jagiellońskim. O nie, nie były to studia związane z informatyką, co wcale nie okazało się przeszkodą. Zaczęłam się doszkalać i doszkalać, robić grafiki i strony www dla znajomych oraz ogłaszać przez internet. Poświęciłam czas na poznawanie javascript, mysql i php. Zetknęłam się z&nbsp;Joomlom, najbardziej rozbudowanym i dającym największe możliwości cms-em, który przez pewien czas był moim głównym narzędziem pracy.</p>" +
  "<p>Ale moje życie to nie tylko strony www. Uwielbiam również bieganie, snowboarding oraz książki.</p>"+
  "<p>Zapraszam serdecznie do obejrzenia mojego portfolio.</p>";

  var mainEng = "<p>Yes, I like deal with computer graphics and webmastering. The first time I came in contact with html and css was during my studies at Jagielloński University. Neither computer graphics nor webmastering were my major but it wasn’t an obstacle for me. I learned much about they, created my first websites and advertised my computer services on the internet during that time. I dedicated a lot of time to becoming familiar with Joomla, the most powerful content management system, and made a few websites on it.</p>"+
  "<p>But my life is not only webmastering. I love jogging, snowboarding and good books.</p>" +
  "<p>I invite you to look at my portfolio and I hope it'll be pleaser.</p>";

  var urlPl = 'demo/colorsGame/index.html';
  var urlEng = 'demo/colorsGame/_index.html';

  var getTranslate = (lang) => {
    switch (lang) {
      case "eng":
          $('.longTextTranslate').html(mainEng);
          $('#insidetext.apps #webmin:first-child a').attr('href', urlEng);
          $('#insidetext.apps #webmin:first-child iframe').attr('src', urlEng);
        break;
      case "pl":
          $('.longTextTranslate').html(mainPl);
          $('#insidetext.apps #webmin:first-child a').attr('href', urlPl);
          $('#insidetext.apps #webmin:first-child iframe').attr('src', urlPl);
        break;
      default:
          $('.longTextTranslate').html(mainPl);
          $('#insidetext.apps #webmin:first-child a').attr('href', urlEng);
          $('#insidetext.apps #webmin:first-child iframe').attr('src', urlEng);
    }
  }

  return {getTranslate: getTranslate}
}
