$(document).ready(function() {
    $('.link').click(function(){
      var subpage = $(this).attr('data-subpage');
      var src = 'subpages/'+ subpage + '/' + subpage +'.php';
      var script = 'subpages/'+ subpage + '/js/' + subpage +'.js';

      if(subpage == 'about' && $(this).attr('id') == 'menu'){
        var script = '';
      }

      $.ajax({
        url: src,
        context: document.body,
      })
        .done(function(responseText){
          $('#text').html(responseText);
          $.getScript(script)
            .always(function(){
              Translator().getTranslation($('.active').attr('data-lang'));
            })
        })
    })
})
