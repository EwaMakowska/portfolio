$(document).ready(function(){

	var contentHeightResize = () => {
		var defaultHeight = '580px';

		if($("#text").hasClass("small")){
			return defaultHeight;
		}
		else {
			var insidetext = $('#insidetext').outerHeight();
			var paddingTop = parseInt($('#text').css('padding-top'));
			var paddingBottom = parseInt($('#text').css('padding-bottom'));
			var height =  insidetext + paddingTop + paddingBottom;
			return	height;
		}
	}
	$('#content, #text, #infbox').css({"height": contentHeightResize()})

	var infBoxWidthResize = () => {
		var width;
		if($(window).width() >= 800){
			width = '30%'
		} else {width = '100%'}
		return width
	}

/*bigger div*/
	$('.bigger').click(function(){

		$('#text').removeClass('small');

		$('#text')
			.animate({
				width: "100%"
			}, 1500, function(){
				$('#content, #text')
					.animate({
						height: contentHeightResize()
					}, 1500)
			})

		$('#infbox')
			.animate({
				height: "50px",
				width: "17%",
				left: "5px",
				borderWidth: 0
			}, 1500)

		$('#menu')
			.animate({
				borderWidth: '1px',
				fontWeight: 800
			}, 1500,
			function(){
				$('#menu')
					.css({
						borderStyle: "solid",
						borderColor: "#666699",
						color: "#993366"
					})
			})
			.fadeOut("slow", function(){
				$('#menu').fadeIn("slow")
			})
});

/*smaller div*/
	$('#menu').click(function(){

		$('#text').addClass('small');

		$('#text')
			.animate({
				width: "67%"
			}, 1500, function(){
				$('#content, #text')
					.animate({
						height: contentHeightResize()
					}, 1500)
			})

		$('#infbox')
			.animate({
				width: infBoxWidthResize(),
				height: '580px',
				left: '0',
				borderWidth: "1px"
			}, 1500)

		$('#menu')
			.animate({
				borderWidth: 0,
				fontWeight: 400
			}, 1500,
			function(){
				$('#menu')
					.css({
						color: "#000000"
					})
			})
	})

})
